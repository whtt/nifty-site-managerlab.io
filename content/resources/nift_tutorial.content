<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">Nift Tutorial</div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<header class="style1">
				<h2>Tutorial to get a better understanding of how Nift works</h2>
			</header>

			<!-- Image -->
			<center>
				<img src="@pathtofile(site/images/teaching.svg)" alt="unDraw teaching illustration" width=300 style="margin-bottom:40px">
			</center>

			<div class="content">
				<p>
					The following tutorial will help you learn how <mono>Nift</mono> works, if you are just taking a look at <mono>Nift</mono> you may prefer the <a href="@pathto(resources/get_started)">getting started guide</a> which will get you up and running much quicker, then come back when you want to have a better understanding of how <mono>Nift</mono> works.
				</p>

				<p>
					The easiest way to learn how <mono>Nift</mono> works is to get your feet wet and hands dirty playing with a simple site. If there is any problems with the following tutorial please file an issue <a href="https://github.com/nifty-site-manager/nifty-site-manager.github.io/issues">here</a> or <a href="https://gitlab.com/nifty-site-manager/nifty-site-manager.gitlab.io/issues">here</a>.
				</p>

				<ol>
					<li>
						Download <a href="https://gitlab.com/nsm-templates/simple-site/-/archive/master/simple-site-master.zip">simple-site-master.zip</a> and extract <mono>simple-site-master</mono>. If you have a BitBucket, GitHub and/or GitLab account then you can import/fork and rename a repository from <a href="https://bitbucket.org/nsm-templates">here</a>, <a href="https://github.com/nsm-templates">here</a> or <a href="https://gitlab.com/nsm-templates">here</a> then clone using <mono>nsm clone repository-url</mono> (Note - You can rename a repository when importing it, rather than forking and renaming, plus on BitBucket and GitHub you cannot remove fork relationships);
					</li>

					<li>
						If you imported/forked a repository on GitHub then in the repository's settings on GitHub for GitHub Pages set the source to <mono>master branch</mono> (there may be some delay before your website goes live);
					</li>

					<li>
						Open a terminal window and change to the <mono>simple-site-master</mono> directory, you will also find it useful to have the <mono>simple-site-master</mono> directory open using your operating system's window manager;
					</li>

					<li>
						Open <mono>template/page.template</mono>, <mono>template/footer.content</mono>, <mono>template/menu.content</mono>, <mono>content/index.content</mono> and <mono>content/about.content</mono> in any text editor you like, take a look at what each file contains;
					</li>

					<li>
						If you ever forget a <mono>Nift</mono> command, enter <mono>nsm commands</mono> into your terminal window;
					</li>

					<li>
						Enter <mono>nsm status</mono> into your terminal window;
					</li>

					<li>
						Enter <mono>nsm info-all</mono>, <mono>nsm info-names</mono> and <mono>nsm status</mono>;
					</li>

					<li>
						The command to have <mono>Nift</mono> track a page is <mono>nsm track page-name page-title template-path</mono>, where <mono>page-title</mono> and <mono>template-path</mono> are optional (default <mono>page-title</mono> is the specified <mono>page-name</mono>, and default <mono>template-path</mono> is <mono>template/page.template</mono>). Enter <mono>nsm track first_page myPageTitle</mono>;
					</li>

					<li>
						We will come back to building pages later in the tutorial, but enter <mono>nsm build-updated</mono>.
					</li>

					<li>
						The page list file <mono>.siteinfo/pages.list</mono> should now contain the following information for pages <mono>about</mono> and <mono>index</mono>;

						<div align="center" style="margin-top:30px">
<pre class="inline rounded">
about
about
template/page.template

first_page
myPageTitle
template/page.template

index
home
template/page.template

</pre>
						</div> <br>
					</li>

					<li>
						Enter <mono>nsm info-all</mono>, <mono>nsm info-names</mono>, <mono>nsm info about</mono>, <mono>nsm info about index</mono> and <mono>nsm status</mono>;
					</li>

					<li>
						Enter <mono>nsm rm first_page</mono> and repeat the last step;
					</li>

					<li>
						Enter <mono>nsm build-updated</mono>;
						<ul>
							<li>
								The directory <mono>site/</mono> should now contain the pages <mono>index.html</mono> and <mono>about.html</mono>. By default users do not have permission to write to page files, this is to prevent users from accidentally modifying a page file when they more than likely intended to modify a content file. Should users want to manually edit a page file they can simply change the file permissions (eg. <mono>chmod +w page-path</mono>); 
							</li>

							<li>
								Another directory <mono>.siteinfo/site/</mono> should have been created containing the info files <mono>index.info</mono> and <mono>about.info</mono>. By default users do not have permission to write to info files, and users should beware of ever doing so. The modification time of info files is how <mono>Nift</mono> knows when pages were last built (actually I am pretty sure <mono>Nift</mono> gets this information from the first line of the information files), furthermore the content inside info files represents page dependencies from the last time pages were built, <u>not necessarily</u> current page dependencies, hence users should not have any reason to modify them;
							</li>
						</ul>
					</li>

					<li>
						Open <mono>site/index.html</mono> in a browser, click through to the about page, and view the source for both pages. Compare what you have locally to a <a href="https://nsm-templates.gitlab.io/simple-site/">pre-built</a> version of <mono>site/index.html</mono> 
					</li>

					<li>
						Enter <mono>nsm status</mono>;
					</li>

					<li>
						Modify <mono>content/index.content</mono>;
					</li>

					<li>
						Enter <mono>cd content/</mono>;
					</li>

					<li>
						Enter <mono>nsm status</mono>, then <mono>nsm build-updated</mono>, then <mono>nsm status</mono> (notice <mono>Nift</mono> can be run from any directory inside the stage directory, stage directory inclusive, obviously as that is where we started! If you are using BitBucket or GitHub then the site directory is set up as a master branch);
					</li>

					<li>
						Enter <mono>cd ../</mono> to change back to the stage directory;
					</li>

					<li>
						Modify <mono>template/footer.content</mono>;
					</li>

					<li>
						Enter <mono>nsm status</mono>, then <mono>nsm build-updated</mono>;
					</li>

					<li>
						Add <mono>\@input(content/badfile.content)</mono> to <mono>content/index.content</mono>;
					</li>

					<li>
						Enter <mono>nsm status</mono>, then <mono>nsm build-updated</mono>, then <mono>nsm status</mono>;
					</li>

					<li>
						Replace <mono>\@input(content/badfile.content)</mono> with <mono>\@input(content/index.content)</mono> in <mono>content/index.content</mono>;
					</li>

					<li>
						Enter <mono>nsm status</mono>, then <mono>nsm build-updated</mono>, then <mono>nsm status</mono>;
					</li>

					<li>
						Remove <mono>\@input(content/index.content)</mono> from <mono>content/index.content</mono>;
					</li>

					<li>
						Enter <mono>nsm status</mono>, then <mono>nsm build-updated</mono>;
					</li>

					<li>
						Add <mono>\$x = \frac{-b \pm \sqrt{b^2-4ac}}{2a}\$</mono> to <mono>content/about.content</mono>;
					</li>

					<li>
						Enter <mono>nsm build-updated</mono> and reload <mono>site/about.html</mono>;
					</li>

					<li>
						Add the following to <mono>content/about.content</mono> without any indenting;
						<div align="center"  style="margin-top:30px">
<pre class="inline prettyprint lang-cpp rounded">
<pre>
ostream& operator<<(ostream &os, const std::vector<std::string> &v)
{
	if(v.size())
		os << v[0];
	for(auto i=1; i<v.size(); i++)
		os << " " << v[i];
	return os;
}
</pre>
</pre>
						</div> <br>
					</li>

					<li>
						Enter <mono>nsm build-updated</mono> and reload <mono>site/about.html</mono>. View the source for <mono>site/about.html</mono> and look at the text inside the pre tags. Notice how every < character has been escaped to \&lt;, <mono>Nift</mono> automatically does this so you can simply paste your code straight into content files, otherwise you would need to manually replace < characters (if you want, add write permissions to <mono>site/about.html</mono> and see what happens when you revert back to using < inside the pre tags);
					</li>

					<li>
						Now try changing the open pre tag in <mono>content/about.content</mono> from <mono>@\<pre></mono> to <mono>@\<pre class="inline rounded prettyprint lang-cpp"></mono>.
					</li>

					<li>
						Enter <mono>nsm build-updated</mono> and reload <mono>site/about.html</mono>. See <mono>site/css/pre.css</mono> for how the simple site styles pre blocks.
					</li>

					<li>
						Enter <mono>nsm mv about test</mono>, check that: <mono>.siteinfo/pages.list</mono> has been updated; both <mono>site/index.html</mono> and <mono>.siteinfo/site/index.info</mono> were removed; and <mono>content/about.content</mono> was moved to <mono>content/test.content</mono>;
					</li>

					<li>
						Enter <mono>nsm cp test about</mono>, check that <mono>.siteinfo/pages.list</mono> has been updated and <mono>content/test.content</mono> was copied to <mono>content/about.content</mono>;
					</li>

					<li>
						Enter <mono>nsm rm test</mono>, check that <mono>content/test.content</mono>, <mono>site/test.html</mono> and <mono>.siteinfo/site/test.info</mono> were all removed. 
					</li>

					<li>
						Enter <mono>nsm untrack index</mono>, check that both <mono>site/index.html</mono> and <mono>.siteinfo/site/index.info</mono> were removed.
					</li>

					<li>
						If you forked, renamed and cloned a repository from BitBucket, GitHub or GitLab then you can build, commit and push changes back to BitBucket, GitHub or GitLab all at the same time using <mono>nsm bcp "commit message"</mono> (note for BitBucket you will need to enter your password twice and for GitHub you will need to enter your username and password twice, this is because of using both a stage and master branch for BitBucket/GitHub, something which is not done for GitLab). There may be some delay before your changes go live.
					</li>
				</ol>
			</div>
		</section>
	</div>
</section>
