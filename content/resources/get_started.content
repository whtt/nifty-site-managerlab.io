<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">Get Started</div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<header class="style1">
				<h2>Guide to getting started using Nift</h2>
			</header>

			<!-- Image -->
			<center>
				<img src="@pathtofile(site/images/teaching.svg)" alt="unDraw teaching illustration" width=300 style="margin-bottom:60px">
			</center>

			<div class="content">
				<p>
					If you are new to <mono>Nift</mono> the following guide will help you get started. To understand how <mono>Nift/nsm</mono> works even better check out the <a href="@pathto(resources/nift_tutorial)">Nift tutorial</a> which will give you a more thorough understanding of how different parts of <mono>Nift</mono> work.
				</p>

				<p>
					The easiest way to learn how <mono>Nift</mono> works is to get your feet wet and hands dirty. If there are any problems with the following guide please file an issue <a href="https://github.com/nifty-site-manager/nifty-site-manager.github.io/issues">here</a> or <a href="https://gitlab.com/nifty-site-manager/nifty-site-manager.gitlab.io/issues">here</a>.
				</p>

				<ol>
					<li>If you haven't already installed <mono>Nift</mono>, you can find install instructions <a href="@pathto(docs/installing_nift)">here</a>;
					<li>Open up a terminal or command prompt/power shell window and enter <mono>nsm commands</mono> to get a list of available commands (check out the <a href="@pathto(docs/nift_commands)">Nift commands</a> page from the documentation for information about <mono>Nift</mono> commands);</li>
					<li>On Bitbucket, GitHub or GitLab import (or fork and rename) an existing site repository template from <a href="@pathto(resources/templates)">here</a>;</li>
					<li>If you used GitHub for the last step, go in to the imported/forked repository's settings, scroll down to GitHub Pages and set the <mono>source</mono> to <mono>master branch</mono> (there may be some delay before your website goes live);</li>
					<li>To view the website remotely, open up a browser of your choice and go to:
					<div align="center">
<pre>
https://[user/team/organisation/group-name].[bitbucket/github/gitlab].io/repo-name
</pre>
					</div>
						Note if you name the repository:
					<div align="center">
<pre>
[user/team/organisation/group-name].[bitbucket/github/gitlab].io
</pre>
					</div>
						then the url will be just:
					<div align="center">
<pre>
https://[user/team/organisation/group-name].[bitbucket/github/gitlab].io
</pre>
					</div></li>
					<li>Clone the site repository locally using <mono>nsm clone <i>clone-url</i></mono>, eg.:
					<div align="center">
<pre>
nsm clone https://gitlab.com/nsm-templates/parchment-site.git;
</pre>
					</div></li>
					<li>To view the website locally, open up one of the pages in the <mono>site</mono> directory with a browser of your choice;</li>
					<li>Try modifying some of the files in the <mono>content</mono> directory and run <mono>nsm build-updated</mono>, then refresh your browser to view the changes (check out the <a href="@pathto(docs/content_files)">content files</a> page from the documentation for information about writing content files);</li>
					<li>Try modifying some of the files in the template directory and run <mono>nsm build-updated</mono>, then refresh your browser to view the changes (check out the <a href="@pathto(docs/template_files)">template files</a> page from the documentation for information about writing template files);</li>
					<li>You can push your changes back to Bitbucket/GitHub/GitLab using <mono>nsm bcp "commit message"</mono> (there may be some delay before your changes go live).</li>
					<li>Check out the <a href="@pathto(docs/hosting_your_site)">hosting your site</a> page from the documentation for information about hosting your site. Especially useful is <a href="https://www.netlify.com/">Netlify</a> for custom domains with auto renewing Let's encrypt certificates or <a href="https://www.heroku.com/">Heroku</a> for free hosting of dynamic websites;</li>
					<li>Check out the <a href="@pathto(docs/displaying_mathematics)">displaying mathematics</a> page from the documentation for information about $\LaTeX$ support for your webpages using MathJax;</li>
					<li>Check out the <a href="@pathto(docs/displaying_code_blocks)">displaying code blocks</a> page from the documentation for information about syntax highlighting support for your webpages using <a href="https://github.com/google/code-prettify">Google Code Prettify</a>;</li>
					<li>Check <a href="@pathto(resources/examples)">here</a> for examples of websites managed and generated using <mono>Nift</mono>;</li>
					<li>Check <a href="@pathto(resources/links)">here</a> for all sorts of useful links.</li>
				</ol>
			</div>
		</section>
	</div>
</section>
