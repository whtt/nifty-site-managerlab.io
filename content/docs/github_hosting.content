<!-- Main -->
<section id="main" class="wrapper style2">
	<div class="title">GitHub Hosting</div>
	<div class="container">
		<!-- Features -->
		<section id="features">
			<header class="style1">
				<h2>How to host a website on GitHub</h2>
			</header>

			<center>
				<a href="https://pages.github.com/"><img src="@pathtofile(site/images/Octorabbit.svg)" alt="GitHub Octocat mascot" width=300 style="max-width: 45%; margin-bottom: 20px"></a>
			</center>

			<div class="content">
				<h3>
					Free hosting on GitHub
				</h3>


				<p>
					Note if you have any dyanmic pages (eg. are using php) then you will need to use an alternative for hosting.
				</p>

				<p>
					Information on GitHub pages may be found <a href="https://pages.github.com/">here</a>, <a href="https://help.github.com/en/categories/github-pages-basics">here</a> and <a href="https://guides.github.com/features/pages/">here</a>.
				</p>

				<h4>
					Importing/Forking an existing website repository
				</h4>

				<p>
					By far the easiest way to host a website on GitHub is to import or fork an existing site repository from GitHub or any other code sharing platform. See <a href="@pathto(resources/templates)">here</a> for a variety of templates you can use to make websites.
				</p>

				<h4>
					Simple GitHub setup
				</h4>
					
				<p>
					This section has simple instructions for hosting your site for free on GitHub. Further below are more advanced instructions for setting up the stage directory and site directory as branches in the same repository.
				</p>

				<h4>
					On GitHub with url:
					<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io
</pre>
					</div>
				</h4>

				<ol>
					<li>
						If you do not already have a GitHub account, you can join <a href="https://github.com/join">here</a>;
					</li>

					<li>
						If you do not already have Git installed on your computer, you can find installation instructions <a href="https://help.github.com/set-up-git-redirect">here</a>; <br> 

						<small><u>Note:</u> If you are new to Git and GitHub, do not confuse yourself about authenticating with GitHub from Git. You can follow the rest of these steps and educate yourself later.</small>
					</li>

					<li>
						Create a new repository called <mono><i>username</i>.github.io</mono>, where <mono><i>username</i></mono> is your GitHub username;
					</li>

					<li>
						Open a terminal window and change to the directory you would like to store your repository in locally;
					</li>

					<li>
						Clone a local copy of your new repository by entering:
						<div align="center" style="margin-top:20px">
<pre class="inline">
git clone https://github.com/username/username.github.io.git
</pre>
						</div>
					</li>

					<li>
						Change into the repository directory by entering <mono>cd <i>username</i>.github.io</mono>;
					</li>

					<li>
						Copy your site files (anything inside the site directory, not the whole stage directory) into the repository directory;
					</li>

					<li>
						Enter the following into your terminal window:
						<div align="center" style="margin-top:20px">
<pre class="inline rounded">
git status
git add .
git commit -m "added site files"
git push origin master
</pre>
						</div> <br>
					</li>

					<li>
						In the repository's settings on GitHub, under GitHub Pages set the source branch to <mono>master branch</mono>;
					</li>

					<li>
						Your site, with url:
						<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io
</pre>
						</div>
						should now be hosted and accessible from the internet.
					</li>
				</ol>
					
				<p>
					To update your site, just repeat the steps following (and including) copying the site files into the repository directory.
				</p>

				<h4>
					On GitHub with url:
					<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io/site-name
</pre>
					</div>
				</h4>

				<ol>
					<li>
						If you do not already have a GitHub account, you can join <a href="https://github.com/join">here</a>;
					</li>

					<li>
						If you do not already have Git installed on your computer, you can find installation instructions <a href="https://help.github.com/set-up-git-redirect">here</a>; <br> 

						<small><u>Note:</u> If you are new to Git and GitHub, do not confuse yourself about authenticating with GitHub from Git. You can follow the rest of these steps and educate yourself later.</small>
					</li>

					<li>
						Create a new repository called <mono><i>site-name</i></mono>, you may choose any name you like;
					</li>

					<li>
						Open a terminal window and change to the directory you would like to store your repository in locally;
					</li>

					<li>
						Clone a local copy of your new repository by entering: 
						<div align="center" style="margin-top:20px">
<pre class="inline">
git clone https://github.com/username/site-name.git
</pre>
						</div>
						where <mono><i>username</i></mono> is your GitHub username;
					</li>

					<li>
						Change into the repository directory by entering <mono>cd <i>site-name</i></mono>;
					</li>

					<li>
						Copy your site files (anything inside the site directory, not the whole stage directory) into the repository directory;
					</li>

					<li>
						Enter the following into your terminal window:
						<div align="center" style="margin-top:20px">
<pre class="inline">
git status
git add .
git commit -m "added site files"
git push origin master
</pre>
						</div> <br>
					</li>

					<li>
						In the repository's settings on GitHub, under GitHub Pages set the source branch to <mono>master branch</mono>;
					</li>

					<li>
						Your site, with url:
						<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io/site-name
</pre>
						</div>
						should now be hosted and accessible from the internet.
					</li>
				</ol>

				<p>
					To update your site, just repeat the steps following (and including) copying the site files into the repository directory.
				</p>


				<h4>
					Advanced GitHub setup
				</h4>

				<p>
					This section has advanced instructions for hosting your site for free on GitHub. We will setup two branches, one for the stage directory and another for the site directory. Locally we will also have the branch for the site directory inside the branch for the stage directory. This is a little confusing at first, but is super useful once it makes sense.
				</p>

				<p>
					Once you already have a repository set up for this and you want to make a new local clone I typically (I may add a command to nsm to automate the following at some point):
				</p>

				<ol>
					<li>clone the repository from GitHub;</li>
					<li>switch to the <mono>master/gh-pages</mono> branch if necessary;</li>
					<li>rename the directory to <mono>site</mono>;</li>
					<li>clone the repository from GitHub again (if the size of your repository is large enough that downloading it twice is slow/tiresome/wasteful then just make a local copy the first time you clone it);</li>
					<li>switch to the <mono>stage</mono> branch if necessary;</li>
					<li>remove the copy of the <mono>site</mono> directory from the <mono>stage</mono> directory/branch and move the <mono>site</mono> directory/branch in to the <mono>stage</mono> directory/branch.</li>
				</ol>

				<p>
					I would love to know if someone finds an easier way to achieve the same result below.
				</p>

				<h4>
					On GitHub with url:
					<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io
</pre>
					</div>
				</h4>

				<ol>
					<li>
						If you do not already have a GitHub account, you can join <a href="https://github.com/join">here</a>;
					</li>

					<li>
						If you do not already have Git installed on your computer, you can find installation instructions <a href="https://help.github.com/set-up-git-redirect">here</a>; <br> 

						<small><u>Note:</u> If you are new to Git and GitHub, do not confuse yourself about authenticating with GitHub from Git. You can follow the rest of these steps and educate yourself later.</small>
					</li>

					<li>
						Create a new repository called <mono><i>username</i>.github.io</mono>, where <mono><i>username</i></mono> is your GitHub username;
					</li>

					<li>
						Open a terminal window and change to the directory you would like to store your repository in locally;
					</li>

					<li>
						Clone a local copy of your new repository by entering:
						<div align="center" style="margin-top:20px">
<pre class="inline">
git clone https://github.com/username/username.github.io.git
</pre>
						</div>
					</li>

					<li>
						Change into the repository directory by entering <mono>cd <i>username</i>.github.io</mono>;
					</li>

					<li>
						Checkout a new branch called <mono>stage</mono> by entering <mono>git checkout -b stage</mono>;
					</li>

					<li>
						Copy your stage files (anything inside the stage directory, including the site directory) into the repository directory;
					</li>

					<li>
						Enter the following into your terminal window:
						<div align="center" style="margin-top:20px">
<pre class="inline rounded">
git status
git add .
git commit -m "setup stage branch"
git push origin stage
</pre>
						</div> <br>
					</li>

					<li>
						Change to the parent of your repository directory by entering <mono>cd ..</mono>
					</li>

					<li>
						Rename your repository directory from <mono><i>username</i>.github.io</mono> to <mono><i>username</i>.github.io-stage</mono>
					</li>

					<li>
						Make a second local clone of your repository by again entering:
						<div align="center" style="margin-top:20px">
<pre class="inline">
git clone https://github.com/username/username.github.io.git
</pre>
						</div>
					</li>

					<li>
						Change into the second local repository directory by again entering <mono>cd <i>username</i>.github.io</mono>;
					</li>

					<li>
						Checkout an orphan branch called <mono>master</mono> by entering <mono>git checkout --orphan master</mono>;
					</li>

					<li>
						Delete everything inside the second local copy of your repository;
					</li>

					<li>
						Enter <mono>git rm -r .</mono>
					</li>

					<li>
						Enter <mono>git status and ensure you get:</mono>
						<div align="center" style="margin-top:20px">
<pre class="inline rounded">
On branch master

Initial commit

nothing to commit (create/copy files and use "git add" to track)
</pre>
						</div><br>
					</li>

					<li>
						Copy the site files (anything inside the site directory, not the stage directory) from the first local copy of your repository (ie. files in <mono><i>username</i>.github.io-stage/site/</mono>) to the second local copy of your repository (ie. place them in <mono><i>username</i>.github.io</mono>, <u>not</u> <mono><i>username</i>.github.io/site/</mono>);
					</li>

					<li>
						Enter the following into your terminal window:
						<div align="center" style="margin-top:20px; margin-bottom: 20px">
<pre class="inline rounded">
git status
git add .
git commit -m "setup master branch"
git push origin master
</pre>
						</div> 
					</li>

					<li>
						In the repository's settings on GitHub, under GitHub Pages set the source branch to <mono>master branch</mono>;
					</li>

					<li>
						Your site, with url:
						<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io
</pre>
						</div>
						should now be hosted and accessible from the internet.
					</li>

					<li>
						Change to the parent of your (second) repository directory by entering <mono>cd ..</mono>
					</li>

					<li>
						Rename your second repository directory from <mono><i>username</i>.github.io</mono> to <mono>site</mono>
					</li>

					<li>
						Move your second repository directory, now named <mono>site</mono>, into your first repository directory (replacing the copy of <mono>site</mono> already in the first repository directory);
					</li>
				</ol>
					
				Now from your local repository, you can manage the site/master branch from inside the site directory, and the stage branch from anywhere else inside the repository. Give it a try, I think you will like it!


				<h4>
					On GitHub with url:
					<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io/site-name
</pre>
					</div>
				</h4>

				<ol>
					<li>
						If you do not already have a GitHub account, you can join <a href="https://github.com/join">here</a>;
					</li>

					<li>
						If you do not already have Git installed on your computer, you can find installation instructions <a href="https://help.github.com/set-up-git-redirect">here</a>; <br> 

						<small><u>Note:</u> If you are new to Git and GitHub, do not confuse yourself about authenticating with GitHub from Git. You can follow the rest of these steps and educate yourself later.</small>
					</li>

					<li>
						Create a new repository called <mono><i>site-name</i></mono>, you may choose any name you like;
					</li>

					<li>
						Open a terminal window and change to the directory you would like to store your repository in locally;
					</li>

					<li>
						Clone a local copy of your new repository by entering:
						<div align="center" style="margin-top:20px">
<pre class="inline">
git clone https://github.com/username/site-name.git
</pre>
						</div>
						where <mono><i>username</i></mono> is your GitHub username;
					</li>

					<li>
						Change into the repository directory by entering <mono>cd <i>site-name</i></mono>;
					</li>

					<li>
						Checkout a new branch called <mono>stage</mono> by entering <mono>git checkout -b stage</mono>;
					</li>

					<li>
						Copy your stage files (anything inside the stage directory, including the site directory) into the repository directory;
					</li>

					<li>
						Enter the following into your terminal window:
						<div align="center" style="margin-top:20px">
<pre class="inline rounded">
git status
git add .
git commit -m "setup stage branch"
git push origin stage
</pre>
						</div> <br>
					</li>

					<li>
						Change to the parent of your repository directory by entering <mono>cd ..</mono>
					</li>

					<li>
						Rename your repository directory from <mono><i>site-name</i></mono> to <mono><i>site-name</i>-stage</mono>;
					</li>

					<li>
						Make a second local clone of your repository by again entering:
						<div align="center" style="margin-top:20px">
<pre class="inline">
git clone https://github.com/username/username/site-name.git
</pre>
						</div>
					</li>

					<li>
						Change into the second local repository directory by again entering <mono>cd <i>site-name</i></mono>;
					</li>

					<li>
						Checkout an orphan branch called <mono>master</mono> by entering <mono>git checkout --orphan master</mono>;
					</li>

					<li>
						Delete everything inside the second local copy of your repository;
					</li>

					<li>
						Enter <mono>git rm -r .</mono>
					</li>

					<li>
						Enter <mono>git status and ensure you get:</mono>
						<div align="center" style="margin-top:20px">
<pre class="inline rounded">
On branch gh-pages

Initial commit

nothing to commit (create/copy files and use "git add" to track)
</pre>
						</div><br>
					</li>

					<li>
						Copy the site files (anything inside the site directory, not the stage directory) from the first local copy of your repository (ie. files in <mono><i>site-name</i>-stage/site/</mono>) to the second local copy of your repository (ie. place them in <mono><i>site-name</i></mono>, <u>not</u> <mono><i>site-name</i>/site/</mono>);
					</li>

					<li>
						Enter the following into your terminal window:
						<div align="center" style="margin-top:20px; margin-bottom: 20px">
<pre class="inline rounded">
git status
git add .
git commit -m "setup gh-pages branch"
git push origin master
</pre>
						</div>
					</li>

					<li>
						In the repository's settings on GitHub, under GitHub Pages set the source branch to <mono>master branch</mono>;
					</li>

					<li>
						Your site, with url:
						<div align="center" style="margin-top:20px">
<pre class="inline">
https://username.github.io/site-name
</pre>
						</div>
						should now be hosted and accessible from the internet.
					</li>

					<li>
						Change to the parent of your (second) repository directory by entering <mono>cd ..</mono>
					</li>

					<li>
						Rename your second repository directory from <mono><i>site-name</i></mono> to <mono>site</mono>
					</li>

					<li>
						Move your second repository directory, now named <mono>site</mono>, into your first repository directory (replacing the copy of <mono>site</mono> already in the first repository directory);
					</li>
				</ol>

				<p>	
					Now from your local repository, you can manage the site/gh-pages branch from inside the site directory, and the stage branch from anywhere else inside the repository. Give it a try, I think you will like it!
				</p>
			</div>
		</section>
	</div>
</section>
